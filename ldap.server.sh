#!/bin/bash

# Taken from:
# https://www.tecmint.com/install-openldap-server-for-centralized-authentication
/

yum -y install openldap compat-openldap openldap-clients openldap-servers openld
ap-servers-sql openldap-devel

systemctl start slapd
systemctl enable slapd
systemctl status slapd

firewall-cmd --add-service=ldap

systemctl restart firewalld

slappasswd -h {SSHA} -s *********** > /tmp/ldappasswd

i=`cat /tmp/ldappasswd`

echo "dn: olcDatabase={0}config,cn=config" > ~/ldaprootpasswd.ldif
echo "changetype: modify" >> ~/ldaprootpasswd.ldif
echo "add: olcRootPW" >> ~/ldaprootpasswd.ldif
echo "olcRootPW: $i" >> ~/ldaprootpasswd.ldif

ldapadd -Y EXTERNAL -H ldapi:/// -f ldaprootpasswd.ldif

cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
chown -R ldap:ldap /var/lib/ldap/DB_CONFIG
systemctl restart slapd

ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif

echo "dn: olcDatabase={1}monitor,cn=config" > ~/ldapdomain.ldif
echo "changetype: modify" >> ~/ldapdomain.ldif
echo "replace: olcAccess" >> ~/ldapdomain.ldif
echo "olcAccess: {0}to * by dn.base=\"gidNumber=0+uidNumber=0,cn=peercred,cn=ext
ernal,cn=auth\"" >> ~/ldapdomain.ldif
echo "  read by dn.base=\"cn=Manager,dc=us-dc,dc=lan\" read by * none" >> ~/ldap
domain.ldif
echo "" >> ~/ldapdomain.ldif

echo "dn: olcDatabase={2}hdb,cn=config" >> ~/ldapdomain.ldif
echo "changetype: modify" >> ~/ldapdomain.ldif
echo "replace: olcSuffix" >> ~/ldapdomain.ldif
echo "olcSuffix: dc=us-dc,dc=lan" >> ~/ldapdomain.ldif
echo "" >> ~/ldapdomain.ldif

echo "dn: olcDatabase={2}hdb,cn=config" >> ~/ldapdomain.ldif
echo "changetype: modify" >> ~/ldapdomain.ldif
echo "replace: olcRootDN" >> ~/ldapdomain.ldif
echo "olcRootDN: cn=Manager,dc=us-dc,dc=lan" >> ~/ldapdomain.ldif
echo "" >> ~/ldapdomain.ldif

echo "dn: olcDatabase={2}hdb,cn=config" >> ~/ldapdomain.ldif
echo "changetype: modify" >> ~/ldapdomain.ldif
echo "add: olcRootPW" >> ~/ldapdomain.ldif
echo "olcRootPW: $i" >> ~/ldapdomain.ldif
echo "" >> ~/ldapdomain.ldif

echo "dn: olcDatabase={2}hdb,cn=config" >> ~/ldapdomain.ldif
echo "changetype: modify" >> ~/ldapdomain.ldif
echo "add: olcAccess" >> ~/ldapdomain.ldif
echo "olcAccess: {0}to attrs=userPassword,shadowLastChange by" >> ~/ldapdomain.l
dif
echo "  dn=\"cn=Manager,dc=us-dc,dc=lan\" write by anonymous auth by self write
by * none" >> ~/ldapdomain.ldif
echo "olcAccess: {1}to dn.base=\"\" by * read" >> ~/ldapdomain.ldif
echo "olcAccess: {2}to * by dn=\"cn=Manager,dc=us-dc,dc=lan\" write by * read" >
> ~/ldapdomain.ldif

ldapmodify -Y EXTERNAL -H ldapi:/// -f ldapdomain.ldif


echo "dn: dc=us-dc,dc=lan" > ~/baseldapdomain.ldif
echo "objectClass: top " >> ~/baseldapdomain.ldif
echo "objectClass: dcObject" >> ~/baseldapdomain.ldif
echo "objectclass: organization" >> ~/baseldapdomain.ldif
echo "o: us-dc lan" >> ~/baseldapdomain.ldif
echo "dc: us-dc" >> ~/baseldapdomain.ldif
echo "" >> ~/baseldapdomain.ldif

echo "dn: cn=Manager,dc=us-dc,dc=lan" >> ~/baseldapdomain.ldif
echo "objectClass: organizationalRole" >> ~/baseldapdomain.ldif
echo "cn: Manager" >> ~/baseldapdomain.ldif
echo "description: Directory Manager" >> ~/baseldapdomain.ldif
echo "" >> ~/baseldapdomain.ldif

echo "dn: ou=People,dc=us-dc,dc=lan" >> ~/baseldapdomain.ldif
echo "objectClass: organizationalUnit" >> ~/baseldapdomain.ldif
echo "ou: People" >> ~/baseldapdomain.ldif
echo "" >> ~/baseldapdomain.ldif

echo "dn: ou=Group,dc=us-dc,dc=lan" >> ~/baseldapdomain.ldif
echo "objectClass: organizationalUnit" >> ~/baseldapdomain.ldif
echo "ou: Group " >> ~/baseldapdomain.ldif

# problem here
# to look for invisible characters
# /\s\+$
ldapadd -x -D cn=Manager,dc=us-dc,dc=lan -W -f ~/baseldapdomain.ldif

useradd ssaleh
echo ********* | passwd ssaleh --stdin

echo "dn: cn=Manager,ou=Group,dc=us-dc,dc=lan" > ldapgroup.ldif
echo "objectClass: top" >> ldapgroup.ldif
echo "objectClass: posixGroup" >> ldapgroup.ldif
echo "gidNumber: 1005" >> ldapgroup.ldif

ldapadd -x  -W -D "cn=Manager,dc=us-dc,dc=lan" -f ldapgroup.ldif

echo "dn: uid=ssaleh,ou=People,dc=us-dc,dc=lan" > ldapuser.ldif
echo "objectClass: top" >> ldapuser.ldif
echo "objectClass: account" >> ldapuser.ldif
echo "objectClass: posixAccount" >> ldapuser.ldif
echo "objectClass: shadowAccount" >> ldapuser.ldif
echo "cn: tecmint" >> ldapuser.ldif
echo "uid: tecmint" >> ldapuser.ldif
echo "uidNumber: 1005" >> ldapuser.ldif
echo "gidNumber: 1005" >> ldapuser.ldif
echo "homeDirectory: /home/ssaleh" >> ldapuser.ldif
echo "userPassword: $i" >> ldapuser.ldif
echo "loginShell: /bin/bash" >> ldapuser.ldif
echo "gecos: ssaleh" >> ldapuser.ldif
echo "shadowLastChange: 0" >> ldapuser.ldif
echo "shadowMax: 0" >> ldapuser.ldif
echo "shadowWarning: 0" >> ldapuser.ldif

ldapadd -x -D cn=Manager,dc=us-dc,dc=lan -W -f  ldapuser.ldif

